---
title: SafeWatch Open Source Code
---

## Safewatch Open Source Code ![git]

The (blockchain privacy engine) is open-source
and is available on

* safewatch [repositories][repos]
* [bitbucket][bbuck]
* [holoGit][hgit]

You might clone the repository using the command:
```sh
git clone git@git.safewatch.xyz:repo/demo.git
```

[repos]: https://swap.safewatch.care/track/
[bbuck]: https://bitbucket.org/safewatch/blockchain-privacy-engine
[hgit]: https://hologit-ml.ipns.dweb.link/


<style>img[alt=git] { display: inline; max-height: 1.4rem; valign: middle; }</style>

[local]: http://localhost.safewatch.care:8088/repo/bitbucket.org/saferoot/
[remote]: https://safewatch.bitbucket.io/

[app]: https://app.safewatch.care/
[demo]: https://demo.safewatch.care/
[track]: https://www.safewatch.care/track/
[SWAP]: https://swap.safewatch.xyz/
[devops]: http://localhost.safewatch.care:8088/repo/bitbucket.org/devops/
[ops]: https://ops.safewatch.xyz/

[live]: http://localhost.safewatch.care:8088/repo/bitbucket.org/SWPoC/_site
[bucket]: https://bitbucket.org/safewatch/SWAP
[code]: https://git.safewatch.xyz/demo.git
[git]: git-icon.svg
[this]: ../saferoot


