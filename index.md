---
title: SafeWatch RootSystem
---

## Safewatch Root 👁

- [demo] [🎬][live]
- [track](http://localhost:8088/repo/bitbucket.org/paladinAI/_site) [📅][track]
- [app] ([🚧:5000](http://0:5000/))
- [SWAP] [📐][bucket]
- [open source](source.html) [![git]][code]
- [devops] [🛠][ops]
- [local] & [remote]

<style>img[alt=git] { display: inline; max-height: 1.4rem; valign: middle; }</style>

[local]: http://localhost.safewatch.care:8088/repo/bitbucket.org/saferoot/
[remote]: https://safewatch.bitbucket.io/

[app]: https://app.safewatch.care/
[demo]: https://demo.safewatch.care/
[track]: https://www.safewatch.care/track/
[SWAP]: https://swap.safewatch.xyz/
[devops]: http://localhost.safewatch.care:8088/repo/bitbucket.org/devops/
[ops]: https://ops.safewatch.xyz/

[live]: http://localhost.safewatch.care:8088/repo/bitbucket.org/SWPoC/_site
[bucket]: https://bitbucket.org/safewatch/SWAP
[code]: https://bitbucket.org/safewatch/demo
[os]: https://git.safewatch.xyz/demo.git

[git]: git-icon.svg
[this]: ../saferoot


