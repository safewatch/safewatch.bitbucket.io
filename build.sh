#

qm=$(ipfs add -w app.js index.md layout.html style.css phone.jpg $0 -Q)
echo "qm: $qm"
pandoc --template layout.html --metadata qm:$qm -t html -o index.html index.md
pandoc --template layout.html -t html -o source.html source.md
qm=$(ipfs add -w index.html app.js style.css phone.jpg qm.log -Q)
echo "--- # qm metafile\nqm: $qm" > qm.yml
tic=$(date +%s%N | cut -c-13) # ms
echo $tic: $qm >> qm.log
gwport=$(ipfs config Addresses.Gateway | cut -d'/' -f 5)
echo "url: http://127.0.0.1:$gwport/ipfs/$qm"

